package com.crucru.replaytv.fragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.crucru.replaytv.R;
import com.crucru.replaytv.activity.TvListActivity;
import com.crucru.replaytv.adapter.ListViewAdapter;
import com.crucru.replaytv.model.ListViewItem;
import com.crucru.replaytv.model.PagingModel;
import com.crucru.replaytv.utils.NetworkUtil;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-08.
 */
public class Fragment04 extends Fragment implements View.OnClickListener {

    private final String TAG = " Fragment04 - ";
    private ProgressDialog mProgressDialog;
    private ArrayList<ListViewItem> listArr;
    private final String baseUrl = "http://alinktv.org";
    //private String baseMidUrl = "/main/category/";
    //private String categoryUrl = "5";

    private String beforeUrl = "/main/search/q/1%7C";
    private String afterUrl = "%7C0/page/1";
    private String keyword = "";

    private ListView listView;
    //private LinearLayout linearLayout;
    //private ArrayList<PagingModel> pagingArr;
    private GetListView gridListView;

    private boolean clickPaging = false;
    private String pagingUrl = "";

    private Button searchBtn;
    private EditText editText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view;
        String model = Build.MODEL.toLowerCase();
        if (model.equals("sph-d720") || model.contains("nexus")) {
            view = inflater.inflate(R.layout.depend, container, false);
        } else {
            view = inflater.inflate(R.layout.fragment04, container, false);
            listView = (ListView)view.findViewById(R.id.fr04_listview);
            //linearLayout = (LinearLayout)view.findViewById(R.id.li_paging);

            gridListView = new GetListView();//.execute();
            //gridListView.execute();

            editText = (EditText)view.findViewById(R.id.fr04_edit);
            searchBtn = (Button)view.findViewById(R.id.fr04_btn);
            searchBtn.setOnClickListener(this);

        }



        return view;

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.fr04_btn :
                String tempKeyword = editText.getText().toString();
                if(tempKeyword.equals("")){
                    Toast.makeText(getActivity(), "검색어를 입력하세요", Toast.LENGTH_SHORT).show();
                } else {

                    try {
                        keyword = URLEncoder.encode(tempKeyword, "utf-8");
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                    ;
                    new GetListView().execute();
                }

                break;
        }
    }

    public class GetListView extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(getActivity());
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();

            /*if(((LinearLayout) linearLayout).getChildCount() > 0){
                ((LinearLayout) linearLayout).removeAllViews();
            }*/
        }

        @Override
        protected Void doInBackground(Void... params) {

            listArr = null;
            listArr = new ArrayList<ListViewItem>();
            //pagingArr = null;
            //pagingArr = new ArrayList<PagingModel>();

            String url = "";

            if(clickPaging){
                url = baseUrl + pagingUrl;
                clickPaging = false;
            } else {
                url = baseUrl + beforeUrl + keyword + afterUrl;
            }

            Document doc = null;

            try {
                doc = Jsoup.connect(url).timeout(10000).userAgent("Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0").get();
                Elements main = doc.select(".list-group");
                Elements divs = main.select("a");
                // 페이징
                //Elements paging = doc.select(".pagination");
                //Elements lis = paging.select("li");

                Elements iframe = doc.select(".list-group-item");

                for(int i=0 ; i<iframe.size() ; i++){
                    String title = iframe.get(i).text();
                    String linkUrl = iframe.get(i).attr("href");

                    listArr.add(new ListViewItem(title, linkUrl));

                    //Log.d(TAG, "title : " + title);
                   // Log.d(TAG, "linkUrl : " + linkUrl);

                }
            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            // 페이징 버튼 동적 생성
            /*for(int i=0 ; i<pagingArr.size() ; i++){
                Button btn = new Button(getActivity());
                btn.setLayoutParams(new LinearLayout.LayoutParams(120, 140));
                btn.setText(pagingArr.get(i).getPageNum());
                btn.setTextSize(10);
                final String tempUrl = pagingArr.get(i).getLinkUrl();

                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "clicked pageBtn : " + tempUrl);
                        clickPaging = true;
                        pagingUrl = tempUrl;
                        if(!pagingUrl.equals("#")){
                            if(pagingUrl.equals("1")){
                                clickPaging = false;
                            }
                            new GetListView().execute();
                        }

                    }
                });

                //linearLayout.addView(btn);
            }*/


            // adapter에 적용
            listView.setAdapter(new ListViewAdapter(getActivity(), getActivity().getLayoutInflater(), listArr));

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(NetworkUtil.getConnectivity(getActivity())){
                        String linkUrl = baseUrl + listArr.get(position).getLinkUrl();
                        //Log.d(TAG, "linkUrl : " + listArr.get(position).getLinkUrl());

                        Intent intent = new Intent(getActivity(), TvListActivity.class);
                        intent.putExtra("linkUrl", linkUrl);

                        startActivity(intent);
                    } else {
                        Toast.makeText(getActivity(), "네트워크 상태를 확인 하세요.", Toast.LENGTH_SHORT).show();
                    }
                }
            });


            mProgressDialog.dismiss();
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        if(gridListView != null){
            gridListView.cancel(true);
        }
    }

}
