package com.crucru.replaytv.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.crucru.replaytv.R;
import com.crucru.replaytv.model.LinkListModel;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Locale;

public class TvListActivity extends AppCompatActivity {
    private final String TAG = " TvListActivity -  ";
    private String linkUrl;
    private LinearLayout linearLayout;
    private ProgressDialog mProgressDialog;
    private ArrayList<LinkListModel> btnArr;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tv_list);

        // 광고
        AdView mAdViewUpper = (AdView)findViewById(R.id.adView_list_upper);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdViewUpper.loadAd(adRequest);

        Intent intent = getIntent();
        linkUrl = (String)intent.getSerializableExtra("linkUrl");

        Log.d(TAG, "linkUrl : " + linkUrl);

        linearLayout = (LinearLayout)findViewById(R.id.li_linklist);


        // async
        new GetLinkList().execute();

    }

    public class GetLinkList extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(TvListActivity.this);
            mProgressDialog.setTitle("리스트를 불러오는 중입니다.");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            btnArr = null;
            btnArr = new ArrayList<LinkListModel>();

            Document doc = null;

            try {
                String[] sort = linkUrl.split("[/]");
                String encoding = URLEncoder.encode(sort[8], "utf-8");
                String url = "";
                for(int i=0 ; i<sort.length ; i++){
                    if(i==8){
                        url += encoding + "/";
                    } else {
                        url += sort[i] + "/";
                    }
                }

                Log.d(TAG, "url : " + url);
                doc = Jsoup.connect(url).timeout(10000).userAgent("Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.4) Gecko/20100101 Firefox/4.0").get();
                Elements as = doc.select(".btn-primary");

                for(int i=0 ; i<as.size() ; i++){
                    String linkText = as.get(i).text();
                    String videoUrl = as.get(i).attr("href");
                    Log.d(TAG, "linkText  : " + linkText);
                    Log.d(TAG, "videoUrl  : " + videoUrl);
                    btnArr.add(new LinkListModel(linkText, videoUrl));
                }
            } catch(Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);

            for(int i=0 ; i<btnArr.size() ; i++){
                Button btn = new Button(TvListActivity.this);
                btn.setWidth(200);
                btn.setHeight(60);
                btn.setText(btnArr.get(i).getLinkText());
                final String videoUrl = btnArr.get(i).getVideoUrl();

                btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d(TAG, "clicked videoUrl : " + videoUrl);

                        Intent intent = new Intent(TvListActivity.this, VideoViewActivity.class);
                        intent.putExtra("videoUrl", videoUrl);
                        startActivity(intent);

                    }
                });

                linearLayout.addView(btn);

                if(i == 5){
                    break;
                }
            }

            mProgressDialog.dismiss();
        }
    }

}
