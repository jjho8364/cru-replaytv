package com.crucru.replaytv.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.crucru.replaytv.R;
import com.crucru.replaytv.model.Fr05ListViewItem;
import com.crucru.replaytv.model.ListViewItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-11.
 */
public class Fr05ListViewAdapter extends BaseAdapter {

    Context context;
    private LayoutInflater inflater;
    private ArrayList<Fr05ListViewItem> listArr;

    public Fr05ListViewAdapter(Context context, LayoutInflater inflater, ArrayList<Fr05ListViewItem> listArr) {
        this.context = context;
        this.inflater = inflater;
        this.listArr = listArr;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        convertView = inflater.inflate(R.layout.fr05listviewitem, null);
        if(convertView != null){

            ImageView img = (ImageView)convertView.findViewById(R.id.fr05_item_img);
            Picasso.with(context).load(listArr.get(position).getImgUrl()).into(img);

        }

        return convertView;
    }


    @Override
    public int getCount() {
        return listArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

}
