package com.crucru.replaytv.model;

/**
 * Created by Administrator on 2016-07-08.
 */
public class ListViewItem {
    String title;
    String linkUrl;

    public ListViewItem(){

    }

    public ListViewItem(String title, String linkUrl) {
        this.title = title;
        this.linkUrl = linkUrl;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }
}
