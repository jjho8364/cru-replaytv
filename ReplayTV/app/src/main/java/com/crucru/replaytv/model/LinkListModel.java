package com.crucru.replaytv.model;

/**
 * Created by Administrator on 2016-07-09.
 */
public class LinkListModel {

    String linkText;
    String videoUrl;

    public LinkListModel(){

    }

    public LinkListModel(String linkText, String videoUrl) {
        this.linkText = linkText;
        this.videoUrl = videoUrl;
    }

    public String getLinkText() {
        return linkText;
    }

    public void setLinkText(String linkText) {
        this.linkText = linkText;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
}
