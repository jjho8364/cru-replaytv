package com.crucru.replaytv.model;

/**
 * Created by Administrator on 2016-07-11.
 */
public class Fr05ListViewItem {

    String imgUrl;
    String videoUrl;

    public Fr05ListViewItem(String imgUrl, String videoUrl) {
        this.imgUrl = imgUrl;
        this.videoUrl = videoUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getVideoUrl() {
        return videoUrl;
    }

    public void setVideoUrl(String videoUrl) {
        this.videoUrl = videoUrl;
    }
}
