package com.crucru.replaytv.model;

/**
 * Created by Administrator on 2016-07-08.
 */
public class PagingModel {

    String linkUrl;
    String pageNum;

    public PagingModel(){

    }

    public PagingModel(String linkUrl, String pageNum) {
        this.linkUrl = linkUrl;
        this.pageNum = pageNum;
    }

    public String getLinkUrl() {
        return linkUrl;
    }

    public void setLinkUrl(String linkUrl) {
        this.linkUrl = linkUrl;
    }

    public String getPageNum() {
        return pageNum;
    }

    public void setPageNum(String pageNum) {
        this.pageNum = pageNum;
    }
}
