package com.crucru.replaytv.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.crucru.replaytv.R;
import com.crucru.replaytv.model.ListViewItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Administrator on 2016-07-08.
 */
public class ListViewAdapter extends BaseAdapter {
    Context context;
    private LayoutInflater inflater;
    private ArrayList<ListViewItem> listArr;

    public ListViewAdapter(Context context, LayoutInflater inflater, ArrayList<ListViewItem> listArr) {
        this.context = context;
        this.inflater = inflater;
        this.listArr = listArr;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        convertView = inflater.inflate(R.layout.listviewitem, null);
        if(convertView != null){
            TextView tv_title = (TextView)convertView.findViewById(R.id.listitem_tv);
            tv_title.setText(listArr.get(position).getTitle());
        }

        return convertView;
    }

    @Override
    public int getCount() {
        return listArr.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
}
